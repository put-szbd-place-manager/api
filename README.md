# place-manager

## Description

Api server for place manager

## Prerequisites

- `Node.js >= 10.0.0`
- Type in your db config in `ormconfig.json`

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
npm run start:prod
```
