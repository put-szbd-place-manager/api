import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, OneToMany } from 'typeorm';
import { IsEmail } from 'class-validator';
import * as crypto from 'crypto';
import { PlacesEntity } from 'places/places.entity';
import { NewsEntity } from 'news/news.entity';
import { CustomOpeningHoursEntity } from 'customOpeningHours/customOpeningHours.entity';
import { DepartmentsEntity } from 'departments/departments.entity';
import { ContactsEntity } from 'contacts/contacts.entity';

@Entity('users')
export class UserEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  @IsEmail()
  email: string;

  @Column()
  password: string;

  @OneToMany(type => PlacesEntity, place => place.user)
  places: PlacesEntity[];

  @OneToMany(type => NewsEntity, news => news.user)
  news: NewsEntity[];

  @OneToMany(type => CustomOpeningHoursEntity, customOpeningHour => customOpeningHour.user)
  customOpeningHours: CustomOpeningHoursEntity[];

  @OneToMany(type => DepartmentsEntity, department => department.user)
  departments: DepartmentsEntity[];

  @OneToMany(type => ContactsEntity, contact => contact.user)
  contacts: ContactsEntity[];

  @BeforeInsert()
  hashPassword() {
    this.password = crypto.createHmac('sha256', this.password).digest('hex');
  }
}