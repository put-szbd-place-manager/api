import { Component, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getRepository } from 'typeorm';
import { validate } from 'class-validator';
import * as crypto from 'crypto';
import * as jwt from 'jsonwebtoken';
import { UserEntity } from './user.entity';
import { CreateUserDto, LoginUserDto, UpdateUserDto } from './dto';
import { SECRET } from '../config';
import { UserRO } from './user.interface';

@Component()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
  ) {}

  async findAll(): Promise<UserEntity[]> {
    return await this.userRepository.find();
  }

  async findOne(loginUserDto: LoginUserDto): Promise<UserEntity> {
    const findOneOptions = {
      email: loginUserDto.email,
      password: crypto.createHmac('sha256', loginUserDto.password).digest('hex'),
    };

    return await this.userRepository.findOne(findOneOptions);
  }

  async create(dto: CreateUserDto): Promise<UserRO> {

    // check uniqueness of username/email
    const { name, email, password } = dto;
    const qb = await getRepository(UserEntity)
      .createQueryBuilder('user')
      .orWhere('user.email = :email', { email });

    const user = await qb.getOne();

    if (user) {
      throw new HttpException({errors: { message: 'User with typed email already exists'}}, HttpStatus.BAD_REQUEST);

    }

    // create new user
    const newUser = new UserEntity();
    newUser.name = name;
    newUser.email = email;
    newUser.password = password;

    const errors = await validate(newUser);
    if (errors.length > 0) {
      const _errors = {username: 'Userinput is not valid.'};
      throw new HttpException({message: 'Input data validation failed', _errors}, HttpStatus.BAD_REQUEST);

    } else {
      const savedUser = await this.userRepository.save(newUser);
      return this.buildUserRO(savedUser);
    }

  }

  async update({ id }, dto: UpdateUserDto): Promise<UserEntity> {
    const existingUser = await this.findByEmail({ email: dto.email });

    if (existingUser) {
      throw new HttpException({errors: { message: 'User with typed email already exists'}}, HttpStatus.BAD_REQUEST);
    }

    const toUpdate = await this.userRepository.findOneById(id);
    toUpdate.password = undefined;

    const updated = Object.assign(toUpdate, dto);
    return await this.userRepository.save(updated);
  }

  async delete({ id }): Promise<void> {
    return await this.userRepository.deleteById(id);
  }

  async findById({ id }): Promise<UserRO>{
    const user = await this.userRepository.findOneById(id);

    if (!user) {
      return;
    }

    return this.buildUserRO(user);
  }

  async findByEmail(user: any): Promise<UserRO>{
    const pulledUser = await this.userRepository.findOne({ email: user.email });
    return { ...pulledUser, password: undefined };
  }

  private buildUserRO(user: UserEntity) {
    const userRO = {
      name: user.name,
      email: user.email,
    };

    return {user: userRO};
  }
}