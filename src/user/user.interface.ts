export class UserData {
  name: string;
  email: string;
}

export class UserRO {
  user: UserData;
}

export class UserROLogin {
  user: UserData;
  token: string;
}