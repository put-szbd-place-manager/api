import { Get, Post, Body, Put, Delete, Param, Controller, UsePipes, HttpException, UseGuards, HttpStatus } from '@nestjs/common';
import { UserService } from './user.service';
import { UserRO, UserROLogin } from './user.interface';
import { CreateUserDto, UpdateUserDto, LoginUserDto } from './dto';
import { User } from './user.decorator';
import { ValidationPipe } from '../shared/pipes/validation.pipe';
import { JwtAuthGuard } from 'auth/guards/jwt-auth.guard';
import { AuthService } from 'auth/auth.service';

@Controller('user')
export class UserController {

  constructor(
    private readonly userService: UserService,
    private readonly authService: AuthService,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  async findMe(@User() user: any): Promise<UserRO> {
    return await this.userService.findByEmail(user);
  }

  @UsePipes(new ValidationPipe())
  @Put()
  async update(@User('id') user: any, @Body() userData: UpdateUserDto) {
    return await this.userService.update(user, userData);
  }

  @UsePipes(new ValidationPipe())
  @Post()
  async create(@Body() userData: CreateUserDto) {
    return this.userService.create(userData);
  }

  @Delete()
  async delete(@User('id') user: any) {
    return await this.userService.delete(user);
  }

  @UsePipes(new ValidationPipe())
  @Post('login')
  async login(@Body() loginUserDto: LoginUserDto): Promise<UserROLogin> {
    const _user = await this.userService.findOne(loginUserDto);

    if (!_user) throw new HttpException({errors: { message: 'Wrong email and/or password'}}, HttpStatus.BAD_REQUEST);
    
    const { id, email, name, password } = _user;
    const token = await this.authService.createToken({
      id,
      email,
      password,
    });
    const { accessToken } = token;
    const user = { email, name };
    return { user, token: accessToken };
  }
}