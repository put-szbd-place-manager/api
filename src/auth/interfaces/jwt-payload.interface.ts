import { IsEmail, IsNotEmpty } from 'class-validator';

export class JwtPayload {
  @IsNotEmpty()
  id: number;

  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsNotEmpty()
  password: string;
}