import { Module } from '@nestjs/common';
import { ContactsController } from './contacts.controller';
import { ContactsService } from './contacts.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ContactsEntity } from './contacts.entity';
import { PlacesEntity } from 'places/places.entity';
import { DepartmentsEntity } from 'departments/departments.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([ContactsEntity]),
    TypeOrmModule.forFeature([PlacesEntity]),
    TypeOrmModule.forFeature([DepartmentsEntity]),
  ],
  controllers: [ContactsController],
  providers: [ContactsService],
})
export class ContactsModule {}
