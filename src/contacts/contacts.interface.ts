import { PlacesData } from 'places/places.interface';
import { DepartmentsData } from 'departments/departments.interface';

export class ContactsData {
  name: string;
  email: string;
  phone: string;
  department: DepartmentsData;
  places: PlacesData[];
}
