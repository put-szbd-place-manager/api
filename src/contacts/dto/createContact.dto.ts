import { IsNotEmpty, MaxLength, IsUrl, IsOptional, IsDate, IsBoolean, ValidateIf, IsDateString, IsEmail, IsNumber, MinLength, IsNumberString } from 'class-validator';

export class CreateContactDto {
  @IsNotEmpty()
  @MaxLength(50)
  readonly name: string;

  @ValidateIf(o => (o.email === null || o.email === undefined || o.phone !== undefined))
  @IsNotEmpty({
    message: 'Phone must be defined if email does not exist',
  })
  @MinLength(7)
  @MaxLength(16)
  @IsNumberString()
  readonly phone: string;

  @ValidateIf(o => (o.phone === null || o.phone === undefined || o.email !== undefined))
  @IsNotEmpty({
    message: 'Email must be defined if phone does not exist',
  })
  @IsEmail()
  @MaxLength(50)
  readonly email: string;

  @IsOptional()
  @IsNotEmpty()
  @IsNumber()
  readonly department_id: number;

  @IsOptional()
  readonly places: number[];
}