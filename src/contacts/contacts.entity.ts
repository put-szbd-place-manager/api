import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, ManyToMany, JoinTable, BeforeInsert, BeforeUpdate, OneToOne } from 'typeorm';
import { UserEntity } from 'user/user.entity';
import { PlacesEntity } from 'places/places.entity';
import { DepartmentsEntity } from 'departments/departments.entity';

@Entity('contacts')
export class ContactsEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 50 })
  name: string;

  @Column({ nullable: true, length: 16 })
  phone: string;

  @Column({ nullable: true, length: 50 })
  email: string;

  @ManyToOne(type => DepartmentsEntity, department => department.contacts)
  @JoinColumn({ name: 'department_id'})
  department: DepartmentsEntity;

  @ManyToOne(type => UserEntity, user => user.contacts)
  @JoinColumn({ name: 'user_id'})
  user: UserEntity;

  @ManyToMany(type => PlacesEntity, place => place.contacts, { onDelete: 'cascade' })
  @JoinTable({
    name: 'contact_place',
    joinColumn: {
      name: 'contact_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'place_id',
      referencedColumnName: 'id',
    },
  })
  places: PlacesEntity[];
}