import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, EntityManager } from 'typeorm';
import { map, omit, merge, find } from 'lodash';
import { ContactsEntity } from './contacts.entity';
import { CreateContactDto } from './dto/createContact.dto';
import { JwtPayload } from 'auth/interfaces/jwt-payload.interface';
import { UserEntity } from 'user/user.entity';
import { UpdateContactDto } from './dto/updateContact.dto';
import { PlacesEntity } from 'places/places.entity';
import { DepartmentsEntity } from 'departments/departments.entity';

interface ContactsPlacesCount {
  contact_id: number;
  places_count: string;
}

@Injectable()
export class ContactsService {
  constructor(
    @InjectRepository(ContactsEntity)
    private readonly contactsRepository: Repository<ContactsEntity>,
    @InjectRepository(PlacesEntity)
    private readonly placesRepository: Repository<PlacesEntity>,
    @InjectRepository(DepartmentsEntity)
    private readonly departmentsRepository: Repository<DepartmentsEntity>,
    private readonly entityManager: EntityManager,
  ) {}

  async findAll(user: JwtPayload) {
    const contacts = await this.contactsRepository.find({
      where: {
        user,
      },
      relations: [
        'places',
        'department',
      ],
      order: {
        id: 'ASC',
      },
    });

    const rawData = await this.entityManager.query(`CALL countPlacesForAllContacts(${user.id})`);
    const countPlacesForContacts: ContactsPlacesCount[] = rawData[0];
    const processedResult = map(
      contacts,
      p => omit(
        merge(
          p,
          find(
            countPlacesForContacts,
            {
              contact_id: p.id,
            },
          ),
        ),
        ['contact_id'],
      ),
    );
    return processedResult;
  }

  async findOne(user: JwtPayload, id: number) {
    const contact = await this.contactsRepository.findOne({
      where: {
        id,
        user,
      },
      relations: [
        'places',
        'department',
      ],
    });

    if (!contact) {
      throw new HttpException({ message: `Contacts with id ${id} does not exist` }, HttpStatus.BAD_REQUEST);
    }

    const rawData = await this.entityManager.query(`CALL countPlacesForAllContacts(${user.id})`);
    const countPlacesForContacts: ContactsPlacesCount[] = rawData[0];

    const processedResult = omit(
      merge(
        contact,
        find(
          countPlacesForContacts,
          {
            contact_id: contact.id,
          },
        ),
      ),
      ['contact_id'],
    );
    return processedResult;
  }

  async create(user: UserEntity, dto: CreateContactDto): Promise<ContactsEntity> {
    const { name, phone, email, department_id, places } = dto;

    const placesEntites = await this.placesRepository.findByIds(places || []);
    const departmentsEntity = await this.departmentsRepository.findOneById(department_id);

    const newContact = new ContactsEntity();

    newContact.name = name;
    newContact.phone = phone;
    newContact.email = email;
    newContact.places = placesEntites;
    newContact.department = departmentsEntity;
    newContact.user = user;

    const savedContact = await this.contactsRepository.save(newContact);
    return { ...savedContact, user: undefined };
  }

  async update(user: UserEntity, id: number, dto: UpdateContactDto): Promise<ContactsEntity> {
    const toUpdate = await this.contactsRepository.findOne({
      id,
      user,
    });

    dto.id = parseInt(dto.id, 10);

    const places = await this.placesRepository.findByIds(dto.places || []);
    const department = await this.departmentsRepository.findOneById(dto.department_id || []);

    if (!toUpdate) {
      throw new HttpException({ message: `Contacts with id ${id} does not exist` }, HttpStatus.BAD_REQUEST);
    }

    const updated = { ...toUpdate, ...dto, places, department: department || null };
    const updatedContacts = await this.contactsRepository.save(updated);

    return { ...updatedContacts, user: undefined };
  }

  async delete(user: UserEntity, id: number) {
    const toDelete = await this.contactsRepository.findOne({
      id,
      user,
    });

    if (!toDelete) {
      throw new HttpException({ message: `Contacts with id ${id} does not exist` }, HttpStatus.BAD_REQUEST);
    }

    await this.contactsRepository.delete(toDelete);

    return 'OK';
  }
}
