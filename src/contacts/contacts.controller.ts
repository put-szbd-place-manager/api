import { Controller, UseGuards, Get, UsePipes, Post, Body, Put, Delete, Param } from '@nestjs/common';
import { ContactsService } from './contacts.service';
import { JwtAuthGuard } from 'auth/guards/jwt-auth.guard';
import { User } from 'user/user.decorator';
import { ValidationPipe } from 'shared/pipes/validation.pipe';
import { CreateContactDto } from './dto/createContact.dto';
import { JwtPayload } from 'auth/interfaces/jwt-payload.interface';
import { UserEntity } from 'user/user.entity';
import { UpdateContactDto } from './dto/updateContact.dto';

@Controller('contacts')
export class ContactsController {
  constructor(private readonly contactsService: ContactsService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  async findAll(@User() user: JwtPayload) {
    return await this.contactsService.findAll(user);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async findOne(@User() user: JwtPayload, @Param('id') id: number) {
    return await this.contactsService.findOne(user, id);
  }

  @UseGuards(JwtAuthGuard)
  @UsePipes(new ValidationPipe())
  @Post()
  async create(@User() user: UserEntity, @Body() placeData: CreateContactDto) {
    return this.contactsService.create(user, placeData);
  }

  @UseGuards(JwtAuthGuard)
  @UsePipes(new ValidationPipe())
  @Put(':id')
  async update(@User() user: UserEntity, @Param('id') id: number, @Body() placeData: UpdateContactDto) {
    return this.contactsService.update(user, id, placeData);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async delete(@User() user: UserEntity, @Param('id') id: number) {
    return this.contactsService.delete(user, id);
  }
}
