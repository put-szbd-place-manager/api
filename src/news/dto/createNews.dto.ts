import { IsNotEmpty, MaxLength, IsUrl, IsOptional } from 'class-validator';

export class CreateNewsDto {
  @IsNotEmpty()
  @MaxLength(50)
  readonly name: string;

  @IsOptional()
  @IsNotEmpty()
  @MaxLength(255)
  readonly content: string;

  @IsOptional()
  readonly places: number[];
}