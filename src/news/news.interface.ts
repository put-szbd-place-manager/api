import { PlacesData } from 'places/places.interface';

export class NewsData {
  name: string;
  content: string;
  places: PlacesData[];
}
