import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, ManyToMany, JoinTable } from 'typeorm';
import { UserEntity } from 'user/user.entity';
import { PlacesEntity } from 'places/places.entity';

@Entity('news')
export class NewsEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 50})
  name: string;

  @Column({ nullable: true, length: 255 })
  content: string;

  @ManyToOne(type => UserEntity, user => user.news)
  @JoinColumn({ name: 'user_id'})
  user: UserEntity;

  @ManyToMany(type => PlacesEntity, place => place.news)
  @JoinTable({
    name: 'news_place',
    joinColumn: {
      name: 'news_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'place_id',
      referencedColumnName: 'id',
    },
  })
  places: PlacesEntity[];
}