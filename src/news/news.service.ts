import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, EntityManager } from 'typeorm';
import { map, omit, merge, find } from 'lodash';
import { NewsEntity } from './news.entity';
import { CreateNewsDto } from './dto/createNews.dto';
import { JwtPayload } from 'auth/interfaces/jwt-payload.interface';
import { UserEntity } from 'user/user.entity';
import { UpdateNewsDto } from './dto/updateNews.dto';
import { PlacesEntity } from 'places/places.entity';

interface NewsPlacesCount {
  news_id: number;
  places_count: string;
}

@Injectable()
export class NewsService {
  constructor(
    @InjectRepository(NewsEntity)
    private readonly newsRepository: Repository<NewsEntity>,
    @InjectRepository(PlacesEntity)
    private readonly placesRepository: Repository<PlacesEntity>,
    private readonly entityManager: EntityManager,
  ) {}

  async findAll(user: JwtPayload) {
    const news = await this.newsRepository.find({
      where: {
        user,
      },
      relations: [
        'places',
      ],
    });

    const rawData = await this.entityManager.query(`CALL countPlacesForAllNews(${user.id})`);
    const countPlacesForNews: NewsPlacesCount[] = rawData[0];
    const processedResult = map(news, p => omit(merge(p, find(countPlacesForNews, { news_id: p.id })), ['news_id']));
    return processedResult;
  }
  
  async findOne(user: JwtPayload, id: number) {
    const news = await this.newsRepository.findOne({
      where: {
        id,
        user,
      },
      relations: [
        'places',
      ],
    });

    if (!news) {
      throw new HttpException({ message: `News with id ${id} does not exist` }, HttpStatus.BAD_REQUEST);
    }

    const rawData = await this.entityManager.query(`CALL countPlacesForAllNews(${user.id})`);
    const countPlacesForNews: NewsPlacesCount[] = rawData[0];

    const processedResult = omit(merge(news, find(countPlacesForNews, { news_id: news.id })), ['news_id']);
    return processedResult;
  }

  async create(user: UserEntity, dto: CreateNewsDto): Promise<NewsEntity> {
    const { name, content, places } = dto;
    
    const placesEntites = await this.placesRepository.findByIds(places);

    const newNews = new NewsEntity();

    newNews.name = name;
    newNews.content = content;
    newNews.user = user;
    newNews.places = placesEntites;

    const savedNews = await this.newsRepository.save(newNews);
    return { ...savedNews, user: undefined };
  }

  async update(user: UserEntity, id: number, dto: UpdateNewsDto): Promise<NewsEntity> {
    const toUpdate = await this.newsRepository.findOne({
      id,
      user,
    });

    const places = await this.placesRepository.findByIds(dto.places);

    if (!toUpdate) {
      throw new HttpException({ message: `News with id ${id} does not exist` }, HttpStatus.BAD_REQUEST);
    }

    const { name, content } = dto;
    const updated = { ...toUpdate, name, content, places };
    const updatedNews = await this.newsRepository.save(updated);

    return { ...updatedNews, user: undefined };
  }

  async delete(user: UserEntity, id: number) {
    const toDelete = await this.newsRepository.findOne({
      id,
      user,
    });

    if (!toDelete) {
      throw new HttpException({ message: `News with id ${id} does not exist` }, HttpStatus.BAD_REQUEST);
    }

    await this.newsRepository.delete(toDelete);

    return 'OK';
  }
}
