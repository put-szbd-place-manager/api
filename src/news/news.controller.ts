import { Controller, UseGuards, Get, UsePipes, Post, Body, Put, Delete, Param } from '@nestjs/common';
import { NewsService } from './news.service';
import { JwtAuthGuard } from 'auth/guards/jwt-auth.guard';
import { User } from 'user/user.decorator';
import { NewsData } from './news.interface';
import { ValidationPipe } from 'shared/pipes/validation.pipe';
import { CreateNewsDto } from './dto/createNews.dto';
import { JwtPayload } from 'auth/interfaces/jwt-payload.interface';
import { UserEntity } from 'user/user.entity';
import { UpdateNewsDto } from './dto/updateNews.dto';

@Controller('news')
export class NewsController {
  constructor(private readonly newsService: NewsService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  async findAll(@User() user: JwtPayload): Promise<NewsData[]> {
    return await this.newsService.findAll(user);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async findOne(@User() user: JwtPayload, @Param('id') id: number): Promise<NewsData> {
    return await this.newsService.findOne(user, id);
  }

  @UseGuards(JwtAuthGuard)
  @UsePipes(new ValidationPipe())
  @Post()
  async create(@User() user: UserEntity, @Body() placeData: CreateNewsDto) {
    return this.newsService.create(user, placeData);
  }

  @UseGuards(JwtAuthGuard)
  @UsePipes(new ValidationPipe())
  @Put(':id')
  async update(@User() user: UserEntity, @Param('id') id: number, @Body() placeData: UpdateNewsDto) {
    return this.newsService.update(user, id, placeData);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async delete(@User() user: UserEntity, @Param('id') id: number) {
    return this.newsService.delete(user, id);
  }
}
