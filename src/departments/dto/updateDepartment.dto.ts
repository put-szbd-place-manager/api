import { IsNotEmpty, MaxLength, IsOptional } from 'class-validator';

export class UpdateDepartmentDto {
  @IsOptional()
  @IsNotEmpty()
  @MaxLength(30)
  readonly name: string;
}