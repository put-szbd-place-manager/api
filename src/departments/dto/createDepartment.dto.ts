import { IsNotEmpty, MaxLength } from 'class-validator';

export class CreateDepartmentDto {
  @IsNotEmpty()
  @MaxLength(30)
  readonly name: string;
}