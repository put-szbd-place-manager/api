import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, ManyToMany, JoinTable, PrimaryColumn, OneToMany } from 'typeorm';
import { UserEntity } from 'user/user.entity';
import { ContactsEntity } from 'contacts/contacts.entity';

@Entity('departments')
export class DepartmentsEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 30 })
  name: string;

  @ManyToOne(type => UserEntity, user => user.departments)
  @JoinColumn({ name: 'user_id' })
  user: UserEntity;

  @OneToMany(type => ContactsEntity, contact => contact.department)
  contacts: ContactsEntity[];
}