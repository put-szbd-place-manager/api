import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { DepartmentsEntity } from './departments.entity';
import { CreateDepartmentDto } from './dto/createDepartment.dto';
import { JwtPayload } from 'auth/interfaces/jwt-payload.interface';
import { UserEntity } from 'user/user.entity';
import { UpdateDepartmentDto } from './dto/updateDepartment.dto';

@Injectable()
export class DepartmentsService {
  constructor(
    @InjectRepository(DepartmentsEntity)
    private readonly departmentsRepository: Repository<DepartmentsEntity>,
  ) {}

  async findAll(user: JwtPayload) {
    return await this.departmentsRepository.find({
      where: {
        user,
      },
      relations: [
        'contacts',
      ],
    });
  }
  
  async findOne(user: JwtPayload, id: number) {
    const department = await this.departmentsRepository.findOne({
      where: {
        id,
        user,
      },
      relations: [
        'contacts',
      ],
    });

    if (!department) {
      throw new HttpException({ message: `Department with id ${id} does not exist` }, HttpStatus.BAD_REQUEST);
    }

    return department;
  }

  async create(user: UserEntity, dto: CreateDepartmentDto): Promise<DepartmentsEntity> {
    const { name } = dto;
    const newDepartments = new DepartmentsEntity();

    newDepartments.name = name;
    newDepartments.user = user;

    const savedDepartments = await this.departmentsRepository.save(newDepartments);
    return { ...savedDepartments, user: undefined };
  }

  async update(user: UserEntity, id: number, dto: UpdateDepartmentDto): Promise<DepartmentsEntity> {
    const toUpdate = await this.departmentsRepository.findOne({
      id,
      user,
    });

    const blockedKey = await this.departmentsRepository.findOne({
      name: dto.name,
      user,
    });

    if (!toUpdate) {
      throw new HttpException({ message: `Departments with id ${id} does not exist` }, HttpStatus.BAD_REQUEST);
    }

    if (blockedKey) {
      throw new HttpException({ message: `Departments with name ${dto.name} already exist` }, HttpStatus.BAD_REQUEST);
    }

    const updated = { ...toUpdate, name: dto.name };
    const updatedDepartments = await this.departmentsRepository.save(updated);

    return { ...updatedDepartments, user: undefined };
  }

  async delete(user: UserEntity, id: number) {
    const toDelete = await this.departmentsRepository.findOne({
      id,
      user,
    });

    if (!toDelete) {
      throw new HttpException({ message: `Departments with id ${id} does not exist` }, HttpStatus.BAD_REQUEST);
    }

    await this.departmentsRepository.delete(toDelete);

    return 'OK';
  }
}
