import { Controller, UseGuards, Get, UsePipes, Post, Body, Put, Delete, Param } from '@nestjs/common';
import { DepartmentsService } from './departments.service';
import { JwtAuthGuard } from 'auth/guards/jwt-auth.guard';
import { User } from 'user/user.decorator';
import { DepartmentsData } from './departments.interface';
import { ValidationPipe } from 'shared/pipes/validation.pipe';
import { CreateDepartmentDto } from './dto/createDepartment.dto';
import { JwtPayload } from 'auth/interfaces/jwt-payload.interface';
import { UserEntity } from 'user/user.entity';
import { UpdateDepartmentDto } from './dto/updateDepartment.dto';

@Controller('departments')
export class DepartmentsController {
  constructor(private readonly departmentsService: DepartmentsService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  async findAll(@User() user: JwtPayload): Promise<DepartmentsData[]> {
    return await this.departmentsService.findAll(user);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async findOne(@User() user: JwtPayload, @Param('id') id: number): Promise<DepartmentsData> {
    return await this.departmentsService.findOne(user, id);
  }

  @UseGuards(JwtAuthGuard)
  @UsePipes(new ValidationPipe())
  @Post()
  async create(@User() user: UserEntity, @Body() departmentData: CreateDepartmentDto) {
    return this.departmentsService.create(user, departmentData);
  }

  @UseGuards(JwtAuthGuard)
  @UsePipes(new ValidationPipe())
  @Put(':id')
  async update(@User() user: UserEntity, @Param('id') id: number, @Body() departmentData: UpdateDepartmentDto) {
    return this.departmentsService.update(user, id, departmentData);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async delete(@User() user: UserEntity, @Param('id') id: number) {
    return this.departmentsService.delete(user, id);
  }
}
