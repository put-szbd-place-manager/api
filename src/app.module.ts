import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { AuthModule } from 'auth/auth.module';
import { PlacesModule } from './places/places.module';
import { AuthService } from 'auth/auth.service';
import { NewsModule } from 'news/news.module';
import { CustomOpeningHoursModule } from 'customOpeningHours/customOpeningHours.module';
import { DepartmentsModule } from 'departments/departments.module';
import { ContactsModule } from 'contacts/contacts.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    AuthModule,
    UserModule,
    PlacesModule,
    NewsModule,
    CustomOpeningHoursModule,
    DepartmentsModule,
    ContactsModule,
  ],
  controllers: [AppController],
  providers: [AppService, AuthService],
})
export class AppModule {
  constructor(private readonly connection: Connection) {}
}
