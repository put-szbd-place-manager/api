import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, ManyToMany, BeforeInsert, BeforeUpdate } from 'typeorm';
import { UserEntity } from 'user/user.entity';
import { NewsEntity } from 'news/news.entity';
import { CustomOpeningHoursEntity } from 'customOpeningHours/customOpeningHours.entity';
import moment = require('moment');
import { PlacesEntity } from 'places/places.entity';

@Entity('opening_hours')
export class OpeningHoursEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  day: number;

  @Column()
  closed: boolean;

  @Column({ nullable: true, type: 'time' })
  opens_at: string | Date;
  
  @Column({ nullable: true, type: 'time' })
  closes_at: string | Date;

  @ManyToOne(type => PlacesEntity, place => place.openingHours)
  @JoinColumn({ name: 'place_id'})
  place: PlacesEntity;

  @BeforeInsert()
  @BeforeUpdate()
  parseData() {
    if (!this.closed) {
      this.opens_at = moment(this.opens_at, 'HH:mm').format('HH:mm');
      this.closes_at = moment(this.closes_at, 'HH:mm').format('HH:mm');
    } else {
      this.opens_at = null;
      this.closes_at = null;
    }
  }
}