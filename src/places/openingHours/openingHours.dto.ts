import {
  IsNotEmpty,
  IsBoolean,
  ValidateIf,
  IsDateString,
  IsNumber,
  Min,
  Max,
  ValidationArguments,
  ValidationOptions,
  registerDecorator,
} from 'class-validator';

const moment = require('moment');

export function IsBeforeHour(
  property: string,
  validationOptions?: ValidationOptions,
) {
  return (object: object, propertyName: string) => {
    registerDecorator({
      name: 'IsBeforeHour',
      target: object.constructor,
      propertyName,
      constraints: [property],
      options: validationOptions,
      validator: {
        validate(value: any, args: ValidationArguments) {
          const [relatedPropertyName] = args.constraints;
          const relatedValue = (args.object as any)[relatedPropertyName];
          const parsedValue = moment(value, 'HH:mm');
          const greaterValue = moment(relatedValue, 'HH:mm');
          return (
            parsedValue.isValid() &&
            greaterValue.isValid() &&
            parsedValue.isBefore(greaterValue)
          ); // you can return a Promise<boolean> here as well, if you want to make async validation
        },
      },
    });
  };
}

export function IsHour(
  validationOptions?: ValidationOptions,
) {
  return (object: object, propertyName: string) => {
    registerDecorator({
      name: 'IsHour',
      target: object.constructor,
      propertyName,
      options: validationOptions,
      validator: {
        validate(value: any) {
          const parsedValue = moment(value, 'HH:mm');
          return parsedValue.isValid();
        },
      },
    });
  };
}

export class OpeningHoursDto {
  @IsNotEmpty()
  @IsNumber()
  @Min(1)
  @Max(7)
  readonly day: number;

  @IsNotEmpty()
  @IsBoolean()
  readonly closed: boolean;

  @ValidateIf(o => o.closed === false)
  @IsHour()
  @IsBeforeHour('closes_at', {
    message: 'Opens at is after closes at',
  })
  readonly opens_at: Date;

  @ValidateIf(o => o.closed === false)
  @IsHour()
  readonly closes_at: Date;
}
