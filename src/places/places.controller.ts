import { Controller, UseGuards, Get, UsePipes, Post, Body, Put, Delete, Param } from '@nestjs/common';
import { PlacesService } from './places.service';
import { JwtAuthGuard } from 'auth/guards/jwt-auth.guard';
import { User } from 'user/user.decorator';
import { PlacesData } from './places.interface';
import { ValidationPipe } from 'shared/pipes/validation.pipe';
import { CreatePlaceDto } from './dto/createPlace.dto';
import { JwtPayload } from 'auth/interfaces/jwt-payload.interface';
import { UserEntity } from 'user/user.entity';
import { UpdatePlaceDto } from './dto/updatePlace.dto';

@Controller('places')
export class PlacesController {
  constructor(private readonly placesService: PlacesService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  async findAll(@User() user: JwtPayload): Promise<PlacesData[]> {
    return await this.placesService.findAll(user);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async findOne(@User() user: JwtPayload, @Param('id') id: number): Promise<PlacesData> {
    return await this.placesService.findOne(user, id);
  }

  @UseGuards(JwtAuthGuard)
  @UsePipes(new ValidationPipe())
  @Post()
  async create(@User() user: UserEntity, @Body() placeData: CreatePlaceDto) {
    return this.placesService.create(user, placeData);
  }

  @UseGuards(JwtAuthGuard)
  @UsePipes(new ValidationPipe())
  @Put(':id')
  async update(@User() user: UserEntity, @Param('id') id: number, @Body() placeData: UpdatePlaceDto) {
    return this.placesService.update(user, id, placeData);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async delete(@User() user: UserEntity, @Param('id') id: number) {
    return this.placesService.delete(user, id);
  }
}
