import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, ManyToMany, OneToMany } from 'typeorm';
import { UserEntity } from 'user/user.entity';
import { NewsEntity } from 'news/news.entity';
import { CustomOpeningHoursEntity } from 'customOpeningHours/customOpeningHours.entity';
import { OpeningHoursEntity } from './openingHours/openingHours.entity';
import { ContactsEntity } from 'contacts/contacts.entity';

@Entity('places')
export class PlacesEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 50 })
  name: string;

  @Column({ length: 100 })
  address: string;

  @Column({ nullable: true, length: 100 })
  website: string | null;

  @OneToMany(type => OpeningHoursEntity, openingHour => openingHour.place)
  openingHours: OpeningHoursEntity[];

  @ManyToOne(type => UserEntity, user => user.places)
  @JoinColumn({ name: 'user_id'})
  user: UserEntity;

  @ManyToMany(type => NewsEntity, news => news.places)
  news: NewsEntity[];

  @ManyToMany(type => CustomOpeningHoursEntity, customOpeningHour => customOpeningHour.places)
  customOpeningHours: CustomOpeningHoursEntity[];

  @ManyToMany(type => ContactsEntity, contact => contact.places)
  contacts: ContactsEntity[];
}