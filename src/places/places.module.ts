import { Module } from '@nestjs/common';
import { PlacesController } from './places.controller';
import { PlacesService } from './places.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PlacesEntity } from './places.entity';
import { OpeningHoursEntity } from './openingHours/openingHours.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([PlacesEntity]),
    TypeOrmModule.forFeature([OpeningHoursEntity]),
  ],
  controllers: [PlacesController],
  providers: [PlacesService],
})
export class PlacesModule {}
