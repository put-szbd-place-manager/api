import { IsNotEmpty, MaxLength, IsUrl, IsOptional, ArrayMinSize, ArrayMaxSize, ValidateNested } from 'class-validator';
import { OpeningHoursDto } from 'places/openingHours/openingHours.dto';
import { Type } from 'class-transformer';

export class UpdatePlaceDto {
  @IsOptional()
  @IsNotEmpty()
  @MaxLength(50)
  readonly name: string;

  @IsOptional()
  @IsNotEmpty()
  @MaxLength(100)
  readonly address: string;

  @IsOptional()
  @IsNotEmpty()
  @MaxLength(100)
  @IsUrl()
  readonly website: string | null;

  @IsOptional()
  @IsNotEmpty()
  @ArrayMinSize(7)
  @ArrayMaxSize(7)
  @ValidateNested()
  @Type(() => OpeningHoursDto)
  readonly openingHours: OpeningHoursDto[];
}