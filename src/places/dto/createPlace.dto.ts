import { IsNotEmpty, MaxLength, IsUrl, IsOptional, ArrayMinSize, ArrayMaxSize, ValidateNested } from 'class-validator';
import { OpeningHoursDto } from 'places/openingHours/openingHours.dto';
import { Type } from 'class-transformer';

export class CreatePlaceDto {
  @IsNotEmpty()
  @MaxLength(50)
  readonly name: string;

  @IsNotEmpty()
  @MaxLength(100)
  readonly address: string;

  @IsOptional()
  @IsNotEmpty()
  @MaxLength(100)
  @IsUrl()
  readonly website: string | null;

  @IsNotEmpty()
  @ArrayMinSize(7)
  @ArrayMaxSize(7)
  @ValidateNested({ always: true, each: true })
  @Type(() => OpeningHoursDto)
  readonly openingHours: OpeningHoursDto[];
}