export class PlacesData {
  id: number;
  name: string;
  address: string;
  website: string;
}
