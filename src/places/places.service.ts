import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PlacesEntity } from './places.entity';
import { UserData } from 'user/user.interface';
import { PlaceDto, CreatePlaceDto } from './dto/createPlace.dto';
import { validate } from 'class-validator';
import { transformAndValidate } from 'class-transformer-validator';
import { JwtPayload } from 'auth/interfaces/jwt-payload.interface';
import { UserEntity } from 'user/user.entity';
import { UpdatePlaceDto } from './dto/updatePlace.dto';
import { OpeningHoursEntity } from './openingHours/openingHours.entity';
import { plainToClass, plainToClassFromExist } from 'class-transformer';
import { OpeningHoursDto } from './openingHours/openingHours.dto';
import { ValidationPipe } from 'shared/pipes/validation.pipe';

@Injectable()
export class PlacesService {
  constructor(
    @InjectRepository(PlacesEntity)
    private readonly placesRepository: Repository<PlacesEntity>,
    @InjectRepository(OpeningHoursEntity)
    private readonly openingHoursRepository: Repository<OpeningHoursEntity>,
  ) {}

  async findAll(user: JwtPayload) {
    return await this.placesRepository.find({
      where: {
        user,
      },
      relations: [
        'openingHours',
      ],
    });
  }
  
  async findOne(user: JwtPayload, id: number) {
    const place = await this.placesRepository.findOne({
      where: {
        id,
        user,
      },
      relations: [
        'openingHours',
      ],
    });

    if (!place) {
      throw new HttpException({ message: `Place with id ${id} does not exist` }, HttpStatus.BAD_REQUEST);
    }

    return place;
  }

  async create(user: UserEntity, dto: CreatePlaceDto) {
    // create new user
    const { name, address, website, openingHours } = dto;
    const newPlace = new PlacesEntity();
    newPlace.name = name;
    newPlace.address = address;
    newPlace.website = website;
    newPlace.user = user;
    newPlace.openingHours = openingHours.map(openingHour => {
      const openingHourEntity = new OpeningHoursEntity();
      openingHourEntity.day = openingHour.day;
      openingHourEntity.closed = openingHour.closed;
      openingHourEntity.closes_at = openingHour.closes_at;
      openingHourEntity.opens_at = openingHour.opens_at;
      return openingHourEntity;
    });

    const savedPlace = await this.placesRepository.save(newPlace);
    savedPlace.openingHours.forEach(async hour => {
      hour.place = { ...savedPlace, openingHours: undefined };
      hour = await this.openingHoursRepository.save(hour);
    });

    const savedOpeningHours = savedPlace.openingHours.map(hour => ({
      day: hour.day,
      closed: hour.closed,
      closes_at: hour.closes_at,
      opens_at: hour.opens_at,
    }));

    return { ...savedPlace, openingHours: savedOpeningHours, user: undefined };
  }

  async update(user: UserEntity, id: number, dto: UpdatePlaceDto) {
    const { name, address, website } = dto;

    const toUpdate = await this.placesRepository.findOne({
      where: {
        id,
        user,
      },
      relations: [
        'openingHours',
      ],
    });

    toUpdate.openingHours = toUpdate.openingHours.map(openingHour => {
      const hourToUpdate = dto.openingHours.find(hour => hour.day === openingHour.day);
      if (hourToUpdate) {
        openingHour.closed = hourToUpdate.closed;
        openingHour.opens_at = hourToUpdate.opens_at;
        openingHour.closes_at = hourToUpdate.closes_at;
      }
      return openingHour;
    });

    if (!toUpdate) {
      throw new HttpException({ message: `Place with id ${id} does not exist` }, HttpStatus.BAD_REQUEST);
    }

    const updated = { ...toUpdate, name, address, website };
    const updatedPlace = await this.placesRepository.save(updated);

    toUpdate.openingHours.forEach(hour => this.openingHoursRepository.save(hour));

    return { ...updatedPlace, user: undefined };
  }

  async delete(user: UserEntity, id: number) {
    const toDelete = await this.placesRepository.findOne({
      id,
      user,
    });

    if (!toDelete) {
      throw new HttpException({ message: `Place with id ${id} does not exist` }, HttpStatus.BAD_REQUEST);
    }

    await this.placesRepository.delete(toDelete);

    return 'OK';
  }
}
