import { IsNotEmpty, MaxLength, IsUrl, IsOptional, IsBoolean, ValidateIf, IsDateString, registerDecorator, ValidationOptions } from 'class-validator';
import { IsBeforeHour, IsHour } from 'places/openingHours/openingHours.dto';
import moment = require('moment');

export function IsDateShort(
  validationOptions?: ValidationOptions,
) {
  return (object: object, propertyName: string) => {
    registerDecorator({
      name: 'IsDateShort',
      target: object.constructor,
      propertyName,
      options: validationOptions,
      validator: {
        validate(value: any) {
          const parsedValue = moment(value, 'YYYY-MM-DD');
          return parsedValue.isValid();
        },
      },
    });
  };
}

export class CreateCustomOpeningHoursDto {
  @IsNotEmpty()
  @MaxLength(50)
  readonly name: string;

  @IsOptional()
  @IsNotEmpty()
  @MaxLength(255)
  readonly description: string;

  @IsNotEmpty()
  @IsDateShort()
  readonly date: string;

  @IsNotEmpty()
  @IsBoolean()
  readonly closed: boolean;

  @ValidateIf(o => o.closed === false)
  @IsHour()
  @IsBeforeHour('closes_at', {
    message: 'Opens at is after closes at',
  })
  readonly opens_at: string;

  @ValidateIf(o => o.closed === false)
  @IsHour()
  readonly closes_at: string;

  @IsOptional()
  readonly places: number[];
}