import { IsNotEmpty, MaxLength, IsUrl, IsOptional, IsDate, ValidateIf, IsBoolean } from 'class-validator';
import { IsBeforeHour, IsHour } from 'places/openingHours/openingHours.dto';
import { IsDateShort } from './createCustomOpeningHours.dto';

export class UpdateCustomOpeningHoursDto {
  id: number;

  @IsOptional()
  @IsNotEmpty()
  @MaxLength(50)
  readonly name: string;

  @IsOptional()
  @IsNotEmpty()
  @MaxLength(255)
  readonly description: string;

  @IsOptional()
  @IsNotEmpty()
  @IsDateShort()
  readonly date: string;

  @IsOptional()
  @IsNotEmpty()
  @IsBoolean()
  closed: boolean;

  @IsOptional()
  @ValidateIf(o => o.closed === false)
  @IsHour()
  @IsBeforeHour('closes_at', {
    message: 'Opens at is after closes at',
  })
  opens_at: string;

  @IsOptional()
  @ValidateIf(o => o.closed === false)
  @IsHour()
  closes_at: string;

  @IsOptional()
  readonly places: number[];
}