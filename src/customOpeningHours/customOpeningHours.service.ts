import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, EntityManager } from 'typeorm';
import { map, omit, merge, find } from 'lodash';
import { CustomOpeningHoursEntity } from './customOpeningHours.entity';
import { CreateCustomOpeningHoursDto } from './dto/createCustomOpeningHours.dto';
import { JwtPayload } from 'auth/interfaces/jwt-payload.interface';
import { UserEntity } from 'user/user.entity';
import { UpdateCustomOpeningHoursDto } from './dto/updateCustomOpeningHours.dto';
import { PlacesEntity } from 'places/places.entity';

interface CustomOpeningHoursPlacesCount {
  customOpeningHour_id: number;
  places_count: string;
}

interface FormattedHours {
  id: number;
  formattedHours: string;
}

@Injectable()
export class CustomOpeningHoursService {
  constructor(
    @InjectRepository(CustomOpeningHoursEntity)
    private readonly customOpeningHoursRepository: Repository<CustomOpeningHoursEntity>,
    @InjectRepository(PlacesEntity)
    private readonly placesRepository: Repository<PlacesEntity>,
    private readonly entityManager: EntityManager,
  ) {}

  async findAll(user: JwtPayload) {
    const customOpeningHours = await this.customOpeningHoursRepository.find({
      where: {
        user,
      },
      relations: [
        'places',
      ],
    });

    const formattedHours: FormattedHours[] = await this.customOpeningHoursRepository.manager.query(`
      SELECT id, getFormattedHours(closed, opens_at, closes_at) as formattedHours
      FROM custom_opening_hours
      WHERE user_id = ${user.id}
    `);

    const rawData = await this.entityManager.query(`CALL countPlacesForAllCustomOpeningHours(${user.id})`);
    const countPlacesForCustomOpeningHours: CustomOpeningHoursPlacesCount[] = rawData[0];
    const mergedResult = map(
      customOpeningHours,
      p => merge(
        p,
        find(
          formattedHours,
          {
            id: p.id,
          },
        ),
      ),
    );

    const processedResult = map(
      mergedResult,
      p => omit(
        merge(
          p,
          find(
            countPlacesForCustomOpeningHours,
            {
              custom_opening_hour_id: p.id,
            },
          ),
        ),
        ['custom_opening_hour_id'],
      ),
    );

    return processedResult;
  }
  
  async findOne(user: JwtPayload, id: number) {
    const customOpeningHour = await this.customOpeningHoursRepository.findOne({
      where: {
        id,
        user,
      },
      relations: [
        'places',
      ],
    });

    if (!customOpeningHour) {
      throw new HttpException({ message: `CustomOpeningHour with id ${id} does not exist` }, HttpStatus.BAD_REQUEST);
    }

    const [formattedHours]: FormattedHours[] = await this.customOpeningHoursRepository.manager.query(`
      SELECT id, getFormattedHours(closed, opens_at, closes_at) as formattedHours
      FROM custom_opening_hours
      WHERE id = ${id} AND user_id = ${user.id}
    `);

    const rawData = await this.entityManager.query(`CALL countPlacesForAllCustomOpeningHours(${user.id})`);
    const countPlacesForCustomOpeningHours: CustomOpeningHoursPlacesCount[] = rawData[0];

    const mergedResult = merge(
      customOpeningHour,
      formattedHours,
    );

    const processedResult = omit(
      merge(
        mergedResult,
        find(
          countPlacesForCustomOpeningHours,
          {
            custom_opening_hour_id: customOpeningHour.id,
          },
        ),
      ),
      ['custom_opening_hour_id'],
    );
    return processedResult;
  }

  async create(user: UserEntity, dto: CreateCustomOpeningHoursDto): Promise<CustomOpeningHoursEntity> {
    const { name, date, closed, opens_at, closes_at, description, places } = dto;
    
    const placesEntites = await this.placesRepository.findByIds(places);

    const newCustomOpeningHours = new CustomOpeningHoursEntity();

    newCustomOpeningHours.name = name;
    newCustomOpeningHours.description = description;
    newCustomOpeningHours.date = date;
    newCustomOpeningHours.closed = closed;
    newCustomOpeningHours.opens_at = opens_at;
    newCustomOpeningHours.closes_at = closes_at;
    newCustomOpeningHours.places = placesEntites;
    newCustomOpeningHours.user = user;
    
    const savedCustomOpeningHours = await this.customOpeningHoursRepository.save(newCustomOpeningHours);
    return { ...savedCustomOpeningHours, user: undefined };
  }

  async update(user: UserEntity, id: number, dto: UpdateCustomOpeningHoursDto): Promise<CustomOpeningHoursEntity> {
    const toUpdate = await this.customOpeningHoursRepository.findOne({
      id,
      user,
    });

    dto.id = parseInt(dto.id, 10);

    const places = await this.placesRepository.findByIds(dto.places);

    if (!toUpdate) {
      throw new HttpException({ message: `CustomOpeningHours with id ${id} does not exist` }, HttpStatus.BAD_REQUEST);
    }

    const updated = { ...toUpdate, ...dto, places };
    const updatedCustomOpeningHours = await this.customOpeningHoursRepository.save(updated);

    return { ...updatedCustomOpeningHours, user: undefined };
  }

  async delete(user: UserEntity, id: number) {
    const toDelete = await this.customOpeningHoursRepository.findOne({
      id,
      user,
    });

    if (!toDelete) {
      throw new HttpException({ message: `CustomOpeningHours with id ${id} does not exist` }, HttpStatus.BAD_REQUEST);
    }

    await this.customOpeningHoursRepository.delete(toDelete);

    return 'OK';
  }
}
