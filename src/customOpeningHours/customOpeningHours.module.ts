import { Module } from '@nestjs/common';
import { CustomOpeningHoursController } from './customOpeningHours.controller';
import { CustomOpeningHoursService } from './customOpeningHours.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CustomOpeningHoursEntity } from './customOpeningHours.entity';
import { PlacesEntity } from 'places/places.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([CustomOpeningHoursEntity]),
    TypeOrmModule.forFeature([PlacesEntity]),
  ],
  controllers: [CustomOpeningHoursController],
  providers: [CustomOpeningHoursService],
})
export class CustomOpeningHoursModule {}
