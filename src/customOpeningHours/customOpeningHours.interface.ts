import { PlacesData } from 'places/places.interface';

export class CustomOpeningHoursData {
  name: string;
  description: string;
  date: Date;
  closed: boolean;
  opens_at: Date;
  closes_at: Date;
  places: PlacesData[];
}
