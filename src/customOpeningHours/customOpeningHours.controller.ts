import { Controller, UseGuards, Get, UsePipes, Post, Body, Put, Delete, Param } from '@nestjs/common';
import { CustomOpeningHoursService } from './customOpeningHours.service';
import { JwtAuthGuard } from 'auth/guards/jwt-auth.guard';
import { User } from 'user/user.decorator';
import { ValidationPipe } from 'shared/pipes/validation.pipe';
import { CreateCustomOpeningHoursDto } from './dto/createCustomOpeningHours.dto';
import { JwtPayload } from 'auth/interfaces/jwt-payload.interface';
import { UserEntity } from 'user/user.entity';
import { UpdateCustomOpeningHoursDto } from './dto/updateCustomOpeningHours.dto';

@Controller('customOpeningHours')
export class CustomOpeningHoursController {
  constructor(private readonly customOpeningHoursService: CustomOpeningHoursService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  async findAll(@User() user: JwtPayload) {
    return await this.customOpeningHoursService.findAll(user);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async findOne(@User() user: JwtPayload, @Param('id') id: number) {
    return await this.customOpeningHoursService.findOne(user, id);
  }

  @UseGuards(JwtAuthGuard)
  @UsePipes(new ValidationPipe())
  @Post()
  async create(@User() user: UserEntity, @Body() placeData: CreateCustomOpeningHoursDto) {
    return this.customOpeningHoursService.create(user, placeData);
  }

  @UseGuards(JwtAuthGuard)
  @UsePipes(new ValidationPipe())
  @Put(':id')
  async update(@User() user: UserEntity, @Param('id') id: number, @Body() placeData: UpdateCustomOpeningHoursDto) {
    return this.customOpeningHoursService.update(user, id, placeData);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async delete(@User() user: UserEntity, @Param('id') id: number) {
    return this.customOpeningHoursService.delete(user, id);
  }
}
