import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, ManyToMany, JoinTable, BeforeInsert, BeforeUpdate } from 'typeorm';
import { UserEntity } from 'user/user.entity';
import { PlacesEntity } from 'places/places.entity';
import moment = require('moment');

@Entity('custom_opening_hours')
export class CustomOpeningHoursEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 50 })
  name: string;

  @Column({ nullable: true, length: 255 })
  description: string;

  @Column({ type: 'date' })
  date: string | Date;

  @Column()
  closed: boolean;

  @Column({ nullable: true, type: 'time' })
  opens_at: string | Date;
  
  @Column({ nullable: true, type: 'time' })
  closes_at: string | Date;
  
  @ManyToOne(type => UserEntity, user => user.customOpeningHours)
  @JoinColumn({ name: 'user_id'})
  user: UserEntity;

  @ManyToMany(type => PlacesEntity, place => place.customOpeningHours, { onDelete: 'cascade' })
  @JoinTable({
    name: 'custom_opening_hour_place',
    joinColumn: {
      name: 'custom_opening_hour_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'place_id',
      referencedColumnName: 'id',
    },
  })
  places: PlacesEntity[];

  @BeforeInsert()
  @BeforeUpdate()
  parseData() {
    this.date = moment(this.date).format('YYYY-MM-DD');
    if (!this.closed) {
      this.opens_at = moment(this.opens_at, 'HH:mm').format('HH:mm');
      this.closes_at = moment(this.closes_at, 'HH:mm').format('HH:mm');
    } else {
      this.opens_at = null;
      this.closes_at = null;
    }
  }
}